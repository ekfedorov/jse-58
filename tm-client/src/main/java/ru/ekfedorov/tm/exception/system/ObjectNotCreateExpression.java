package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public class ObjectNotCreateExpression extends AbstractException {

    public ObjectNotCreateExpression() {
        super("Error object is  not create...");
    }

}
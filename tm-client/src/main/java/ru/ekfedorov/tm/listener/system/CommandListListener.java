package ru.ekfedorov.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CommandListListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show program commands.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "commands";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@commandListListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.commandName() != null) {
                System.out.println(listener.commandName() + " - " + listener.commandDescription());
            }
        }
    }

}

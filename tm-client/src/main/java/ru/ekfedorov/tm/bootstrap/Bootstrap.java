package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.listener.AbstractListener;
import ru.ekfedorov.tm.component.FileScanner;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.util.SystemUtil;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Getter
@Setter
@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

    @Autowired
    public AbstractListener[] commands;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    private void displayWelcome() {
        loggerService.debug("***           TEST          ***");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
    }

    private void init() {
        initPID();
        initFileScanner();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable String command = null;
        for (@NotNull final AbstractListener listener : listeners) {
            if (arg.equals(listener.commandArg())) command = listener.commandName();
        }
        if (command == null) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String param = args[0];
        parseArg(param);
        return true;
    }

    private void process() {
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            try {
                System.out.println();
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void run(final String... args) {
        displayWelcome();
        init();
        if (parseArgs(args)) System.exit(0);
        process();
    }

}

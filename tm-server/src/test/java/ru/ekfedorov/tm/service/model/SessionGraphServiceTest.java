package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.api.service.model.ISessionGraphService;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.cofiguration.ServerConfiguration;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.SessionGraph;
import ru.ekfedorov.tm.service.TestUtil;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class SessionGraphServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final ISessionGraphService sessionService = context.getBean(ISessionGraphService.class);


    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        context.getBean(EntityManagerFactory.class).createEntityManager();
    }

    @After
    public void after() {
        context.getBean(EntityManagerFactory.class).close();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<SessionGraph> sessions = new ArrayList<>();
        final SessionGraph session1 = new SessionGraph();
        final SessionGraph session2 = new SessionGraph();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertTrue(sessionService.findOneById(session1.getId()).isPresent());
        Assert.assertTrue(sessionService.findOneById(session2.getId()).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findOneById(session.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final SessionGraph session1 = new SessionGraph();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertTrue(sessionService.contains(sessionId));
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        sessionService.clear();
        final List<SessionGraph> sessions = new ArrayList<>();
        final SessionGraph session1 = new SessionGraph();
        final SessionGraph session2 = new SessionGraph();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final SessionGraph session1 = new SessionGraph();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertNotNull(sessionService.findOneById(sessionId));
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionService.findOneById(sessionId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        final String sessionId = session.getId();
        sessionService.removeOneById(sessionId);
        Assert.assertFalse(sessionService.findOneById(sessionId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        sessionService.remove(session);
        Assert.assertNotNull(sessionService.findOneById(session.getId()));
    }

}

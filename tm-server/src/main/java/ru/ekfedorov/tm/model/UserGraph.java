package ru.ekfedorov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Role;
import org.hibernate.annotations.Cache;
import ru.ekfedorov.tm.listener.LoggerEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_user")
@EntityListeners(LoggerEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGraph extends AbstractGraphEntity {

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Column
    private boolean locked = false;

    @Column
    @Nullable
    private String login;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjectGraph> projects = new ArrayList<>();

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Nullable
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SessionGraph> sessions = new ArrayList<>();

    @Nullable
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TaskGraph> tasks = new ArrayList<>();

}

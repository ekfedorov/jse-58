package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.endpoint.IUserEndpoint;
import ru.ekfedorov.tm.api.service.dto.ISessionService;
import ru.ekfedorov.tm.api.service.dto.IUserService;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.dto.User;
import ru.ekfedorov.tm.exception.empty.EmailIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.empty.PasswordIsEmptyException;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws LoginIsEmptyException, PasswordIsEmptyException, EmailIsEmptyException {
        userService.create(login, password, email);
    }

    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException {
        sessionService.validate(session);
        return userService.findByLogin(login).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserOneBySession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws NullSessionException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new NullSessionException();
        return userService.findOneById(session.getUserId()).orElse(null);
    }

    @Override
    @WebMethod
    public void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws AccessDeniedException, NullSessionException {
        sessionService.validate(session);
        if (session == null) throw new NullSessionException();
        userService.setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) throws AccessDeniedException, NullSessionException {
        sessionService.validate(session);
        if (session == null) throw new NullSessionException();
        userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}

package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.endpoint.IAdminEndpoint;

import ru.ekfedorov.tm.api.service.dto.ISessionService;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IProjectGraphService projectGraphService;

    @NotNull
    @Autowired
    private ITaskGraphService taskGraphService;

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        projectGraphService.clear();
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        taskGraphService.clear();
    }

    @Override
    @WebMethod
    public List<Session> listSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        return sessionService.findAll();
    }

}

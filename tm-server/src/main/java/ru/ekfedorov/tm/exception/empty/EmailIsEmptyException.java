package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class EmailIsEmptyException extends AbstractException {

    public EmailIsEmptyException() {
        super("Error! Email is empty...");
    }

}

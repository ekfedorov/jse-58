package ru.ekfedorov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IGraphRepository;
import ru.ekfedorov.tm.model.UserGraph;

import java.util.List;
import java.util.Optional;

public interface IUserGraphRepository extends IGraphRepository<UserGraph> {

    void clear();

    @NotNull
    List<UserGraph> findAll();

    @NotNull
    Optional<UserGraph> findByLogin(@NotNull String login);

    @NotNull
    Optional<UserGraph> findOneById(@Nullable String id);

    void removeByLogin(@NotNull String login);

    void removeOneById(@Nullable String id);

}
